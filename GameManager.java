public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager() {
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public String toString() {
		return "--------------------------\nCenter card: " + 
				this.centerCard.toString() + "\nPlayer card: " +
				this.playerCard.toString() + "\n--------------------------";
	}
	
	public void dealCards() {
		this.drawPile.shuffle();
		playerCard = drawPile.drawTopCard();
		centerCard = drawPile.drawTopCard();	
	}
	
	public int getNumberOfCards() {
		return this.drawPile.length();
	}
	
	public int calculatePoints() {
		if (this.playerCard.getValue() == this.centerCard.getValue()) {
			return 4;
		} else if (this.playerCard.getSuit().equals(this.centerCard.getSuit())) {
			return 2;
		} else {
			return -1;
		}
	}
}

