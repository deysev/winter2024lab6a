import java.util.Scanner;

public class LuckyCardGameApp {	
	public static void main (String []args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 0;
		System.out.println("Welcome to Lucky! Card Game!");
		
		while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
			System.out.println("Round: "+round);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			round++;
			System.out.println("Points after round "+round+": "+totalPoints);
			manager.dealCards();
		}
		
		System.out.println("Your final points: " + totalPoints);
		
		if (totalPoints >= 5) {
			System.out.println("YAY! YOU WIN!");
		} else {
			System.out.println("oh no... you LOST");
		}
	}
}